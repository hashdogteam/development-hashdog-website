var addClasstoClass = function(stringClassToFind, classToAdd){
  var elements = document.querySelectorAll(stringClassToFind);
  Array.prototype.forEach.call(elements, function(el, i){
    if (el.classList)
      el.classList.add(classToAdd);
    else
      el.className += ' ' + classToAdd;
  });
}

addClasstoClass('.doggerSecondary','doggerSecondaryAnim')
addClasstoClass('.doggerThirth','doggerThirthAnim')
addClasstoClass('.spin','spinAnim')
addClasstoClass('.shine','shineAnim')
addClasstoClass('.ruby-home','ruby-homeAnim')
addClasstoClass('.dogger-home','dogger-homeAnim')