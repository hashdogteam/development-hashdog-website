function checkVisible() {
  $("#team").each(function() {
    $("#team").visible() ? ($(".overlay").addClass("show"), $("#team .container").addClass("show")) : ($(".overlay").removeClass("show"), $("#team .container").removeClass("show"))
  })
}

function setHeight() {
  windowHeight = $(window).innerHeight(), $("#home").css("height", windowHeight)
}

$(window).load(function() {
  $(".loader").addClass("hideme")
}),

$(document).on("ready", function() {
  $("a[href*=#]:not([href=#])").click(function() {
      if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
        var e = $(this.hash);
        if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"), e.length) return $("html,body").animate({
          scrollTop: e.offset().top
        }, 1e3), !1
      }
    }),

  $("a.menu-mobile, .menu-tablet, .showing-menu").on("click", function() {
      $(".showing-menu").toggleClass("active"),
      $(".helper-blur").toggleClass("blur"),
      $("body").toggleClass("hidden-scroll")
    }),

    function(e, t, n, o, a, i, s) {
      e.GoogleAnalyticsObject = a, e[a] = e[a] || function() {
        (e[a].q = e[a].q || []).push(arguments)
      }, e[a].l = 1 * new Date, i = t.createElement(n), s = t.getElementsByTagName(n)[0], i.async = 1, i.src = o, s.parentNode.insertBefore(i, s)
    }(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-49934871-1", "auto"), ga("send", "pageview"), $("body").is(".uiux, .work, .index") && $(".tooltip-tipsy").tipsy({
      gravity: "n"
    }),

    $(".modal, .close").on("click", function(e) {
      e.preventDefault(), $(".modal").fadeOut(), $(".backover").fadeOut()
    }),

    $("#form-contact").on("submit", function(e) {
      e.preventDefault(),
      $(".submit span").fadeOut(),
      $(".loader-form").fadeIn();
      var t = $("input.name").val(),
        n = $("input.email").val(),
        o = $("textarea.message").val();
      $.ajax({
        dataType: "jsonp",
        url: "https://getsimpleform.com/messages/ajax?form_api_token=753eda28e62735804d851ea4fce3cf53",
        data: {
          name: t,
          email: n,
          message: o
        }
      }).success(function(e) {
        $(".loader-form").fadeOut(), $(".submit span").fadeIn(), $(".modal").fadeIn(), $(".backover").fadeIn(), $("form").find("input[type=text], input[type=email], textarea").val("")
      }).error(function(e) {})
    }),

    $(".list p").on("click", function(e) {
      e.preventDefault(), $(this).closest(".list").toggleClass("mobile-foot-show")
    }),

    $(".btn-menu").on("click", function(e) {
      e.preventDefault(),
      $(".float-menu").hasClass("show") || $(".float-menu").hasClass("hide") ? (
        $(".float-menu").toggleClass("show"),
        $(".float-menu").toggleClass("hide")
        )
      : $(".float-menu").hasClass("start") && ($(".float-menu").addClass("show"), $(".float-menu").removeClass("start")),
       $(this).toggleClass("move"), $(".menu-overlay").toggleClass("show"), $(".btn-menu-container").toggleClass("move")
    }),

    $(".helperClick, .menu-overlay").on("click", function(e) {
      e.preventDefault(),
      $(".btn-menu").toggleClass("move"),
      $(".btn-menu-container").toggleClass("move"),
      $(".menu-overlay").toggleClass("show"),
      $(".float-menu").addClass("hide").removeClass("show")
    })
}), $.fn.visible = function(e) {
  var t = $(this),
    n = $(window),
    o = n.scrollTop(),
    a = o + n.height(),
    i = t.offset().top,
    s = i + t.height(),
    l = e === !0 ? s : i,
    c = e === !0 ? i : s;
  return a >= c && l >= o
}, $(document).ready(function(e) {
  checkVisible()
}), $(window).scroll(function(e) {
  checkVisible()
});
var windowHeight;
windowHeight = $(window).innerHeight(), heightMenu = $(window).innerHeight() / 5, $(window).scroll(function() {
  $(window).scrollTop() > heightMenu && $(".nav, .logo-rocks, .logo-mobile").addClass("stick"), $(window).scrollTop() < heightMenu && $(".nav, .logo-rocks, .logo-mobile").removeClass("stick")
});
var windowHeight;
setHeight(), $(window).resize(function() {
  setHeight()
}), $(document).ready(function() {
  new Swiper(".swiper-container", {
    direction: "horizontal",
    loop: !0,
    watchOverflow: !0,
    pagination: {
      el: ".swiper-pagination",
      clickable: !0
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  })
});