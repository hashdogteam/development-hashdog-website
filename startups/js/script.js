$(document).ready(function() {

  $(document).on('click', '.learn-more-container', function() {
    $(document).scrollTo('#learning', 800);
  });

  $(document.body).on('inview', '.inview-block, .item-list, .img-effect, .bg-arrow', function(ev, inview){
    var _this = $(this);
    if(inview){
      requestAnimationFrame(function(){
        _this.addClass('inview');
      });
    }else{
      requestAnimationFrame(function(){
        _this.removeClass('');
      });
    }
  });

});

$(window).load(function() {
  $('.loader').addClass('hideme');
  $('.headline strong').addClass('headlineAnim');
  $('.home-img').addClass('home-imgAnim');
  $('.stella').addClass('stellaAnim');
});