/*
 * hashdog
 */
(function(pub, $, undefined) {

  /**
   * Default options
   */
  var defaults = {
      section: 'home',
      root: '',
      lang: 'fr',
  };
  
  /**
   * Actual options, set using the setOptions method
   */
  var o = defaults;

  /**
   * Set the options for this module
   *
   * @param {object} Options based on the default options structure
   */
  pub.setOptions = function(opts)
  {
    o = $.extend(true, {}, defaults, opts);
  };
  
  // Private variables
  var $document,
    windowWidth,
    scrollCache,
    windowHeight;
    
  var prepareElementsCache = function()
  {
    $document = $(document);
    windowHeight = $(this).height();
    windowWidth = $(this).width();

  };



  /**
   * Manage resizing for this specific section
   */
  var resizeManager = function()
  {
    windowHeight = $(window).height();
    windowWidth = $(window).width();
    if(windowWidth > 1340){
      for(var pElement in o.parallaxElements){
        var current = $(o.parallaxElements[pElement]);
        current.css("marginTop", current.data("css", current.data("initialPosition")));
        current.data("initialOffsetTop", current.offset().top);
        //the number of pixels each element will have to move in order to reach initial position at mouse scroll.
        var onEachScrollAdd = o.parallaxScrollSpeed;
        current.data("parallaxEffectValue", onEachScrollAdd);
        var addedScrollValue = o.parallaxIntensity;
        current.css("marginTop", (addedScrollValue + "px"))
      }
    }else{
      for(var pElement in o.parallaxElements){
        var current = $(o.parallaxElements[pElement]);
        current.css("marginTop", current.data("css", current.data("initialPosition")));
      }
    }
  };
    
  var initHome = function() {

    /**
     *Save initial position before moving it with a parallax.
     */
    for(var pElement in o.parallaxElements){
      var current = $(o.parallaxElements[pElement]);
      current.data("initialPosition", parseInt(current.css("marginTop")));
      current.data("initialOffsetTop", current.offset().top);
      //the number of pixels each element will have to move in order to reach initial position at mouse scroll.
      var onEachScrollAdd = o.parallaxScrollSpeed;
      current.data("parallaxEffectValue", onEachScrollAdd);
      var addedScrollValue = o.parallaxIntensity;
      current.css("marginTop", (addedScrollValue + "px"))
    }

    $(document).click(function() {
      $(document).trigger("scroll");
    })

    $(".learn-more-container").click(function(){
      $('html, body').animate({
        scrollTop: $("#hashdog-logo-sticky").offset().top
      }, 700);
    });

    $(".sticky-header").mouseenter(function(){
      requestAnimationFrame(function(){
        $(this).toggleClass('active');
      });
    }).mouseleave(function(){
      requestAnimationFrame(function(){
        $(this).toggleClass('active');
      });
    });

    $(document).scroll(function() {
      var scrollPos = $(document).scrollTop();
      if(scrollPos >= $(".offer-section").offset().top) {
        requestAnimationFrame(function() {
          $(document.documentElement).removeClass('going-down');
        });
      } else {
        requestAnimationFrame(function() {
          $(document.documentElement).addClass('going-down');
        });
      }
    });
  };

  pub.init = function(opts)
  {
    if (opts !== undefined) {
      pub.setOptions(opts);
    }
    
    prepareElementsCache();
    
    // js and css3 transit initialization
    $('.nojs').removeClass('nojs').addClass('js');

    initHome();
    
    $(window).resize(function() {
      resizeManager();
    });
    //$(window).resize();

  };

} (window.hashdog = window.hashdog || {}, jQuery));

var s = false,
  timeout = 0;

jQuery.fn.isMobile = function() {
  return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
};

$(document).ready(function() {
  hashdog.init({
    lang: 'fr',
    section: 'home'
  });

  $(document.body).on('inview', '.inview-block, .item-list, .img-effect', function(ev, inview){
    var _this = $(this);
    if(inview){
      requestAnimationFrame(function(){
        _this.addClass('inview');
      });
    }else{
      requestAnimationFrame(function(){
        _this.removeClass('');
      });
    }
  });

});

function skrollrInit(){
  if($(window).width()>767 && !$(window).isMobile()){
    s = skrollr.init({
      forceHeight:false,
      smoothScrolling:false
    });
  }else{
     s = skrollr.get();
    if(typeof s!="undefined"){
      s.destroy();
      requestAnimationFrame(function(){
        $(document.body).removeAttr('style');
        $(document.documentElement).removeAttr('style');
      });
    }
  }
}

// buffer so we don't activate / destroy skrollr at each resize event
$(window).on('resize', function(){
  window.clearTimeout(timeout);
  timeout = window.setTimeout(skrollrInit, 250);
});